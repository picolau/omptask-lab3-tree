# OmpTask-Lab3-Tree
Neste problema temos que calcular o peso do maior caminho que começa na raíz de uma árvore n-ária (cada nó possui entre 0 e n filhos). 
Cada nó desta árvore possui um id, o id de seu pai, uma chave k >= 0, e uma lista ligada com apontadores para os seus filhos.


O peso de um caminho é dado pelo somatório das chaves em cada nó deste caminho. Como todas as chaves são não-negativas, o maior caminho com certeza termina em uma folha da árvore.


**Entrada:**  
Uma linha com N, o número de nós da árvore  
N chaves. A i-ésima chave pertence ao nó i.  
N linhas com o id do nó pai, número de filhos, ids dos filhos  

**Saída:**  
X, o peso do maior caminho que começa na raíz da árvore

**Exemplo:**  
3  
7 10 1  
0 0  
1 2 0 2  
2 0  

Corresponde à árvore:  

![Example n-tree](https://gitlab.com/brcloud/labs/omptask-lab3-tree/blob/master/example.jpg)

A implementação serial deste problema está no arquivo *maior_caminho_raiz.c*.
Utilize tasks para paralelizar a "parte 2" neste código, tomando cuidado com seções críticas e com a criação das tarefas.

**Dica:**  
For the in dependence-type, if the storage location of at least one of the list items is the same as the
storage location of a list item appearing in a depend clause with an out, inout, or
mutexinoutset dependence-type on a construct from which a sibling task was **previously**
generated, then the generated task will be a dependent task of that sibling task.  

(Especificação do OpenMP 5.0)

#### How to build:

```shell
$ gcc maior_caminho_raiz.c -Wall -fopenmp -o caminho
$ ./caminho < tests/01.in
```

#### TODO:

1. Fork the repository to your own account.
2. Clone your forked repository on your computer.
3. Study the code using your favorite editor.
4. Make a copy of the code (call it 'parallel_maior_caminho.c' for example).
5. Parallelize the code using OpenMP.
7. Compare performance between serial and parallel version. Determine the speedup using the input *05.in*

Anything missing? Ideas for improvements? Make a pull request.

